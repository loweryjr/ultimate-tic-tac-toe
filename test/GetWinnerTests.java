import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class GetWinnerTests {
    void assertWinner(int expectedWinner, int[][] board, int r, int c){
        int actual= Player.get3x3Winner(board, r,c);
        assertEquals(expectedWinner,actual);
    }
    void assertWinner(int expectedWinner, int[][]board){
        assertWinner(expectedWinner,board,0,0);
    }

    @Test
    void emptyBoard(){
        assertWinner(0, new int[][]{
                {0,0,0},
                {0,0,0},
                {0,0,0}
        });
    }
    @Test
    void middleColumnWinner(){
        assertWinner(1, new int[][]{
                {0,1,0},
                {0,1,0},
                {0,1,0}
        });
    }
    @Test
    void winnerPlayer2(){
        assertWinner(2, new int[][]{
                {1,2,2},
                {0,1,2},
                {0,1,2}
        });
    }
    @Test
    void topRowWinner(){
        assertWinner(1, new int[][]{
                {1,1,1},
                {0,2,2},
                {0,0,2}
        });
    }
    @Test
    void middleRowWinner(){
        assertWinner(2, new int[][]{
                {0,1,1},
                {2,2,2},
                {1,0,2}
        });
    }
    @Test
    void bottomRowWinner(){
        assertWinner(1, new int[][]{
                {0,0,0},
                {0,2,1},
                {1,1,1}
        });
    }
    @Test
    void rightDiagonalWinner(){
        assertWinner(2, new int[][]{
                {2,0,0},
                {0,2,1},
                {1,1,2}
        });
    }
    @Test
    void leftDiagonalWinner(){
        assertWinner(2, new int[][]{
                {1,0,2},
                {0,2,1},
                {2,1,2}
        });
    }
    @Test
    void nineByNine(){
        int[][]board={
                {1, 1, 1, 2, 1, 0, 0, 2, 2},
                {0, 0, 2, 0, 2, 0, 2, 1, 1},
                {0, 2, 0, 1, 0, 2, 1, 0, 0},
                {0, 1, 1, 0, 0, 1, 2, 1, 2},
                {2, 2, 2, 0, 1, 2, 2, 1, 2},
                {0, 1, 0, 1, 2, 0, 1, 2, 1},
                {1, 1, 2, 0, 0, 0, 0, 2, 1},
                {0, 0, 2, 0, 0, 0, 1, 2, 0},
                {1, 0, 2, 0, 0, 0, 0, 2, 1}
        };

        assertWinner(1, board, 0,0);
        assertWinner(2, board, 0, 3);
        assertWinner(0, board, 0, 6);
        assertWinner(2, board, 3, 0);
        assertWinner(1, board, 3, 3);
        assertWinner(0, board, 3, 6);
        assertWinner(2, board, 6, 0);
        assertWinner(0, board, 6, 3);
        assertWinner(2, board, 6, 6);
        assertEquals(0,Player.get9x9Winner(board));
    }
    @Test
    void nineByNineWithWinner(){
        int[][]board={
                {1, 1, 1, 2, 1, 0, 0, 2, 1},
                {0, 0, 2, 1, 1, 1, 2, 1, 1},
                {0, 2, 0, 1, 0, 2, 1, 0, 0},
                {0, 1, 1, 0, 0, 1, 2, 1, 2},
                {2, 2, 2, 0, 1, 2, 2, 1, 2},
                {0, 1, 0, 1, 2, 0, 1, 2, 1},
                {1, 1, 2, 0, 0, 0, 0, 2, 1},
                {0, 0, 2, 0, 0, 0, 1, 2, 0},
                {1, 0, 2, 0, 0, 0, 0, 2, 1}
        };
        assertEquals(1, Player.get9x9Winner(board));
    }


}

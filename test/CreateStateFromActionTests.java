import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CreateStateFromActionTests {
    //HAPPY PATH
    @Test
    void blankApplyUpperLeft() {
        int input[][] = new int[][]{
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0},
        };
        int[][] newState = Player.createStateFromAction(1, "0 0", input);
        assertArrayEquals(new int[][]{
                {1, 0, 0},
                {0, 0, 0},
                {0, 0, 0},
        }, newState);
    }

    @Test
    void nonBlankApplyLowerRight() {
        int input[][] = new int[][]{
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 0},
        };
        int[][] newState = Player.createStateFromAction(2, "2 2", input);
        assertArrayEquals(new int[][]{
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 2},
        }, newState);
    }

    @Test
    void inputStateIsCreated() {
        int input[][] = new int[][]{
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 0},
        };
        int[][] newState = Player.createStateFromAction(2, "0 1", input);
        assertNotSame(input, newState);
        assertArrayEquals(new int[][]{
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 0},
        }, input);
    }

    @Test
    void winningBoardAllowsModification() {
        int input[][] = new int[][]{
                {1, 0, 2},
                {0, 1, 0},
                {0, 0, 1},
        };
        int[][] newState = Player.createStateFromAction(2, "1 2", input);

        assertArrayEquals(new int[][]{
                {1, 0, 2},
                {0, 1, 2},
                {0, 0, 1},
        }, newState);
    }

    //SAD PATH
    @Test
    void nonEmptyCellThrowsException() {

        int input[][] = new int[][]{
                {1, 0, 2},
                {0, 1, 1},
                {0, 0, 1},
        };
        assertThrows(RuntimeException.class,
                () -> Player.createStateFromAction(2, "1 2", input));

    }

    @Test
    void outOfBoundsThrowsException() {
        int input[][] = new int[][]{
                {1, 0, 2},
                {0, 1, 1},
                {0, 0, 1},
        };
        assertThrows(RuntimeException.class,
                () -> Player.createStateFromAction(2, "1 3", input));

    }

    @Test
    void nonFormattedActionThrowsException() {
        int input[][] = new int[][]{
                {1, 0, 2},
                {0, 1, 1},
                {0, 0, 1},
        };
        assertThrows(RuntimeException.class,
                () -> Player.createStateFromAction(2, "ghghf", input));

    }

    @Test
    void nullArgsThrowsException() {
        assertThrows(RuntimeException.class,
                () -> Player.createStateFromAction(2, "1 2", null));

    }

    void assert9x9Action(int player, int r, int c, int[][] board) {
        assertNotEquals(player, board[r][c]);

        int[][] actual = Player.createStateFromAction(player, r + " " + c, board);

        for (int ri = 0; ri < board.length; ri++) {
            for (int ci = 0; ci < board[ri].length; ci++) {
                if (ri == r && ci == c) {
                    assertEquals(player, actual[ri][ci]);
                    assertNotEquals(player, board[ri][ci]);
                } else
                    assertEquals(board[ri][ci], actual[ri][ci]);
            }
        }

        assertEquals(player, actual[r][c]);
    }

    @Test
    void nineByNine() {
        int[][] board = {
                {1, 1, 1, 2, 1, 0, 0, 2, 2},
                {0, 0, 2, 0, 2, 0, 2, 1, 1},
                {0, 2, 0, 1, 0, 2, 1, 0, 0},
                {0, 1, 1, 0, 0, 1, 2, 1, 2},
                {2, 2, 2, 0, 1, 2, 2, 1, 2},
                {0, 1, 0, 1, 2, 0, 1, 2, 1},
                {1, 1, 2, 0, 0, 0, 0, 2, 1},
                {0, 0, 2, 0, 0, 0, 1, 2, 0},
                {1, 0, 2, 0, 0, 0, 0, 2, 1}
        };

        assert9x9Action(1, 0, 6, board);
        assert9x9Action(2, 1, 0, board);
        assert9x9Action(1, 8, 4, board);

    }
}
